import React, { Component } from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";

const validate = values => {
  const errors = {};
  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }
  return errors;
};

class SignUp extends Component {
  renderField = ({
    input,
    label,
    type,
    name,
    meta: { touched, error, warning }
  }) => (
    <div>
      <div>
        <input {...input} placeholder={label} name={name} type={type} />
        <br />
        {touched &&
          ((error && <span style={{ color: "red" }}>{error}</span>) ||
            (warning && <span>{warning}</span>))}
      </div>
    </div>
  );

  handleSignupForm = values => {
    console.log("email:", values.email);
  };
  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;
    return (
      <>
        <center>
          <br />
          <h1>Sign Up</h1>
          <br />

          <form onSubmit={handleSubmit(this.handleSignupForm)}>
            <Field
              name="email"
              type="email"
              component={this.renderField}
              label="Email"
            />
            <br />
            <div>
              <button type="submit" disabled={submitting}>
                Submit
              </button>
              <button
                type="button"
                disabled={pristine || submitting}
                onClick={reset}
              >
                Clear
              </button>
            </div>
            <h4>
              Already have account? <a href="#">Log In</a>
            </h4>
          </form>
        </center>
      </>
    );
  }
}

SignUp = reduxForm({
  form: "signUpForm",
  validate
})(SignUp);

export default connect(
  null,
  null
)(SignUp);
